[0;1;32m●[0m httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: [0;1;32mactive (running)[0m since Fri 2021-03-12 19:13:12 EST; 13min ago
     Docs: man:httpd.service(8)
 Main PID: 8117 (httpd)
   Status: "Total requests: 161; Idle/Busy workers 100/0;Requests/sec: 0.196; Bytes served/sec:  51KB/sec"
    Tasks: 278 (limit: 11392)
   Memory: 31.7M
   CGroup: /system.slice/httpd.service
           ├─8117 /usr/sbin/httpd -DFOREGROUND
           ├─8118 /usr/sbin/httpd -DFOREGROUND
           ├─8119 /usr/sbin/httpd -DFOREGROUND
           ├─8120 /usr/sbin/httpd -DFOREGROUND
           ├─8121 /usr/sbin/httpd -DFOREGROUND
           └─8355 /usr/sbin/httpd -DFOREGROUND

Mar 12 19:13:12 localhost.localdomain systemd[1]: Starting The Apache HTTP Server...
Mar 12 19:13:12 localhost.localdomain httpd[8117]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using localhost.localdomain. Set the 'ServerName' directive globally to suppress this message
Mar 12 19:13:12 localhost.localdomain systemd[1]: Started The Apache HTTP Server.
Mar 12 19:13:12 localhost.localdomain httpd[8117]: Server configured, listening on: port 80
